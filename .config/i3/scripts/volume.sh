#!/bin/bash

id=1000
t=2000

function send_notification() {
    volume=$(pamixer --get-volume)

    if [[ $(pamixer --get-mute) == "true" ]]; then
        icon="audio-volume-muted"
    else
        icon="audio-volume-high"
    fi

    #dunstify -a "changevolume" -i $icon -u normal -r $id -t $t -h int:value:$volume $volume%
    dunstify -a "changevolume" -i $icon -u normal -r $id -t $t $volume%
}

case $1 in
up)
    pamixer -i 1
    ;;
down)
    pamixer -d 1
    ;;
mute)
    pamixer -t
    ;;
esac

send_notification
