#!/bin/bash

killall -q polybar

while pgrep -u $UID -x polybar > /dev/null
do
	sleep 1
done

polybar main -c /home/user/.config/polybar/soul/config.ini &
