syntax on

set number

set fillchars+=vert:\│"
highlight VertSplit cterm=NONE

set tabstop=4
set listchars=tab:»\ ,trail:~,space:·

nnoremap <F3> :set list!<CR>

call plug#begin('~/.vim/plugged')

Plug 'scrooloose/nerdtree'
Plug 'ervandew/supertab'

call plug#end()

filetype indent off
